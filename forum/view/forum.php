<?php

$forum = mysql_fetch_object(mysql_query('SELECT * FROM `forum` WHERE `id` = '.intval($_GET['forum'])));

if (!$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = (isset($_GET['create_razdel'])) ? output_text($forum->name, 1, 1, 0, 0, 0).' - создание раздела' : 'Подфорум - '.output_text($forum->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();

    if (isset($_GET['create_razdel'])) {
        include_once 'action/create_razdel.php'; // Создание раздела.
    } elseif (isset($_GET['edit_razdel'])) {
        include_once 'action/edit_razdel.php'; // Редактирование раздела.
    } else {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a>
        </div>
        <?
    }
    if (isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    if (user_access('forum_razd_create')) {
        ?>
        <div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM.'/'.$forum->id ?>/create_razdel.html'>Создать раздел</a></div>
        <?
    }
    $k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_razdels` WHERE `id_forum` = '.$forum->id), 0);
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Разделы в этом подфоруме ещё не созданы.</div>
        <?
    } else {
        ?>
        <table class = 'post'>
            <?
            $razdels = mysql_query('SELECT * FROM `forum_razdels` WHERE `id_forum` = '.$forum->id.' ORDER BY `number` ASC LIMIT '.$start.', '.$set['p_str']);
            while ($razdel = mysql_fetch_object($razdels)) {
                $count_themes = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id), 0);
                ?>
                <tr>
                    <td class = 'icon14'>
                        <img src = '<?= FORUM ?>/icons/razdel.png' alt = '' <?= ICONS ?> />
                    </td>
                    <td class = 'p_t'>
                        <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> (<?= $count_themes ?>)
                    </td>
                    <?
                    if (user_access('forum_razd_edit')) {
                        ?>
                        <td class = 'icon14'>
                            <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/edit_razdel.html'><img src = '<?= FORUM ?>/icons/edit.png' alt = '' <?= ICONS ?> /></a>
                        </td>
                        <?
                    }
                    ?>
                </tr>
                <?
                if ($razdel->description != NULL && $razdel->output == 0) {
                    ?>
                    <tr>
                        <td class = 'p_m' colspan = '3'>
                            <?= output_text($razdel->description, 1, 1, 0, 1, 1) ?>
                        </td>
                    </tr>
                    <?
                } elseif ($razdel->output == 1 && $count_themes > 0) {
                    ?>
                    <tr>
                        <td class = 'p_m' colspan = '3'>
                            <?
                            $themes = mysql_query('SELECT `id`, `name`, `reason_close`, `type` FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id.' ORDER BY `time` DESC LIMIT 3');
                            while ($theme = mysql_fetch_object($themes)) {
                                if ($theme->type == 1) {
                                    $type = '_up';
                                } elseif ($theme->reason_close != NULL) {
                                    $type = '_close';
                                } else {
                                    $type = NULL;
                                }
                                ?>
                                <img src = '<?= FORUM ?>/icons/theme<?= $type ?>.png' alt = '' style = 'width: 16px; height: 16px' /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a><br />
                                <?
                            }
                            ?>
                        </td>
                    </tr>
                    <?
                }
            }
            ?>
        </table>
        <?
        if ($k_page > 1) {
            str(FORUM.'/'.$forum->id.'/', $k_page, $page);
        }
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
}

?>